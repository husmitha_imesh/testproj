const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const fs = require('fs');
const multer = require('multer');
const usrmode = require('../models/User');




/**
 * @swagger
 * /user/adduser:
 *  post:
 *    description: post request for add user
 *    parameters:
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/adminRequest'
 *    responses:
 *      200:
 *        description: Access token returns for adduser
 * definitions:
 *   adminRequest:
 *     properties:
 *       username:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 *       address:
 *         type: string
 */
router.post('/adduser', async (req, res) => {
  //validate the email existance
  const emailexist = await usrmode.findOne({ email: req.body.email });
  if (emailexist) {
    return res.status(400).send('Email already exist');
  }



  let Secrect_message = req.body.password;
  let key = '12345678123456781234567812345678';

  let ciper = crypto.createCipher('aes-256-cbc', key);
  let encryptec = ciper.update(Secrect_message, 'utf8', 'hex');
  encryptec += ciper.final('hex');



  const usrname = `${req.body.username}.jpg`;

  //convert base64 to image

  let base64 = req.body.encodeString;
  const imageBufferData = Buffer.from(base64, 'base64')
  fs.writeFileSync(usrname, imageBufferData)

  //add user
  const adusr = new usrmode({
    username: req.body.username,
    email: req.body.email,
    password: encryptec,
    address: req.body.address,
    images : usrname,
    
  });


  try {
    const savusr = await adusr.save();
    // res.json(savusr);
    const token1 = jwt.sign({ _id: adusr._id, email: adusr.email }, 'abcd');
    res.status(200).send(token1);
  } catch (err) {
    res.json(err);
  }
});

/**
 * @swagger
 * /user/login:
 *   post:
 *      description: post request for login users
 *      produces:
*        - application/json
 *      parameters:
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: A successful token return for login
 * definitions:
 *   loginRequest:
 *     properties:
 *          email:
 *            type: string
 *          password:
 *            type: string
 *
 */
router.post('/login', async (req, res) => {
  
  const user = await usrmode.findOne({ email: req.body.email });
  if (!user) {
    return res.status(400).send('Email not found');
  }
  
  const storedPassword = user.password;

  
  let key = '12345678123456781234567812345678';
  

  let dicipher = crypto.createDecipher('aes-256-cbc', key);
  let decrypted = dicipher.update(storedPassword, 'hex', 'utf8');
  decrypted += dicipher.final('utf8');


  //compare and check password correct
  if (decrypted !== req.body.password) {
    return res.status(400).send('invalid paswword');
  }

  
   try {

   const token = jwt.sign({ _id: user._id }, 'abcd');
  

   res.status(200).send(token);

 

  } catch (err) {
     res.json(err);
   }
});


router.get('/upload', async(res) => {

 

})

module.exports = router;