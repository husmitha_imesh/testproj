const express = require('express');
const mongoose = require('mongoose');
const swaggerUi = require('swagger-ui-express');
const swaggerJsDocs = require('swagger-jsdoc');

const app = express();

const UrRoute = require('./Controllers/UserController');

const bodyparser = require('body-parser');
app.use(bodyparser.urlencoded({extended: true}))
app.use(bodyparser.json());

app.use('/user', UrRoute);



   
const option = {
    swaggerDefinition: {
      info: {
        title: 'User Server',
        description: 'Server Side API For Users',
        contact: {
          name: 'Bawantha'
        },
        server: ['http://localhost:3004']
      }
    },
    apis: ['./Controllers/*.js']
  };

  const swaggerDocs = swaggerJsDocs(option);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));







//databse connection
mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true, useUnifiedTopology: true }, ).then(() => {
    console.log("db connected");
});

// mongoose.connection.once('open', function(){
//     console.log("Connection has been made");
// })





//listening port
app.listen(3004);